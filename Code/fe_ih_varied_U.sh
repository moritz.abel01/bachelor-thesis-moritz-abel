#!/bin/bash

#accesses the singularity container symmps.latest.compressed and performs a groundstate search on the heisenberg model

L=$1 #read in the lattice size as a programm parameter
U=(21.0 21.1 21.2 21.3 21.4) #different values for U
#U=(21.0 21.05 21.1)
#U=(21.42)
#U=(21.47 21.5 21.55 21.6 21.65 21.7 21.75 21.8 21.85 21.9)
t=1.0 # hopping strenght
delta=20.0 # ionic potential strength

 
echo ${U[@]} > /home/moritz.abel01/MPS/hubbard/gs/fe_ih_varied_U_L=${L}.txt

for i in ${U[@]}
do
    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed bash groundstate_hubbard.sh /opt/symmps/mkl/ /home/moritz.abel01/MPS/ $L $i $t $delta

    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed bash meas_groundstate_hubbard.sh /opt/symmps/mkl/

    #mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n/n_hub_real_L=${j}_U=${i}.txt
    #mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down/n_down_hub_real_L=${j}_U=${i}.txt
    #mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up/n_up_hub_real_L=${j}_U=${i}.txt
    #mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/c_dagger_down_hubc_up_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/c_dagger_down_c_up/c_dagger_down_c_up=${j}_U=${i}.txt
    #mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/c_dagger_up_hubc_down_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/c_dagger_up_c_down/c_dagger_up_c_down=${j}_U=${i}.txt
    mv /home/moritz.abel01/MPS/hubbard/gs/global_operators_real /home/moritz.abel01/MPS/hubbard/gs/BO_parameter_fsc_L=${j}_U=${i}.txt

    rm /home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_*
done

rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_*
rm /home/moritz.abel01/MPS/hubbard/gs/global_operators_imag