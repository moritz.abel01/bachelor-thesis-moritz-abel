import numpy as np
import matplotlib.pyplot as plt
import seaborn
import sys
from scipy.optimize import curve_fit

#the main programm for creating the plots. This works for the Peierls substitution and for the quenches,
#depending on the parameters that are passed

fntsz = 15

plt.rc('font', size=10) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=10) # fontsize of the tick labels
plt.rc('ytick', labelsize=10) # fontsize of the tick labels
plt.rc('legend', fontsize=10) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title
if (len(sys.argv) == 3):
    tag = ''
elif (len(sys.argv) == 4):
    tag = sys.argv[3]
else:
    print(f"len(sys.argv) = {len(sys.argv)} is not accepted")
    sys.exit(1)

L=int(sys.argv[1]) # the system size is passed as an argument of the programm
U=21.42
model = sys.argv[2] # pass p for Peierls substitution and q for quench
mask = [i for i in range(L+2) if i > 1]

if(model == 'p'):
    namestring = 'peierls'
elif(model == 'q'):
    namestring = 'quench'
else: 
    print('unknown model identifyer passed. quitting...')
    sys.exit(1)

if (model == 'p'):
    t = np.loadtxt(f"teo_{namestring}/E_L={L}_U={U}{tag}.txt", delimiter = " ", usecols = 0)
    
    ################################################## energy #################################################################
    E_of_t = np.loadtxt(f"teo_{namestring}/E_L={L}_U={U}{tag}.txt", delimiter = " ", usecols = 2)

    #print(t)
    #print(E_of_t)

    plt.figure()
    plt.grid(which = 'major', alpha = 0.8, color = '#666666')
    plt.minorticks_on()
    plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
    plt.scatter(t, E_of_t, marker=".") #type: ignore
    plt.plot(t, E_of_t)
    plt.xlabel("time $\\tau$", fontsize=fntsz)
    plt.ylabel("energy $E$", fontsize=fntsz)
    plt.xlim(0, 30)
    plt.axvline(x=20, color = 'grey', label = "$\\tau=20$")
    plt.legend()
    plt.savefig(f"Plots/{namestring}/E_of_t_L={L}_U={U}{tag}.png", bbox_inches='tight')

    ################################################## fit a polynomial to the energy after the Peierls Pulse #################
    t_start = 15
    dt = 0.1
    start_data = int(t_start/dt)
    fit_data = E_of_t[start_data:]
    fit_time = t[start_data:]

    # define fit function
    def f(x, a, b, c, d):
        return a*x**3 + b*x**2 + c*x + d
    
    params, params_err = curve_fit(f, fit_time, fit_data)

    plt.figure()
    plt.grid(which = 'major', alpha = 0.8, color = '#666666')
    plt.minorticks_on()
    plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
    plt.scatter(t, E_of_t, marker=".", color = "tab:blue", zorder=1) #type: ignore
    plt.plot(fit_time, f(fit_time, *params), zorder=2, color='tab:red', linewidth=2)
    plt.text(x = 15, y = -10.0, s=f'a={params[0]}, b={params[1]}, c={params[2]}, d={params[3]}', wrap=True, bbox={'facecolor':'white', 'alpha':0.5}, backgroundcolor='white')
    plt.xlabel("time $t$", fontsize=fntsz)
    plt.ylabel("energy $E$", fontsize=fntsz)
    plt.savefig(f"Plots/{namestring}/E_of_t_error_L={L}_U={U}{tag}.png", bbox_inches = 'tight')
#load the time for the right model
t = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}{tag}.txt", delimiter = " ", usecols = 0)


BO_of_t = np.loadtxt(f"teo_{namestring}/BO_parameter/BO_L={L}_U={U}{tag}.txt", delimiter = " ", usecols = 2)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.scatter(t, BO_of_t, marker=".") #type:ignore
plt.plot(t, BO_of_t)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("Bond order parameter $\\langle \\hat B \\rangle$", fontsize=fntsz)
plt.savefig(f"Plots/{namestring}/BO_of_t_L={L}_U={U}{tag}.png", bbox_inches = 'tight')

################################################## local densities #############################################################
n_of_t = np.loadtxt(f"teo_{namestring}/local_operators/n_hub_real_L={L}_U={U}{tag}.txt", delimiter = " ", usecols = mask)
plt.figure()
seaborn.heatmap(n_of_t)
plt.savefig(f"Plots/{namestring}/n_of_t_{namestring}_L={L}_U={U}{tag}.png")
print(n_of_t.shape)



################################################## global current ##############################################################
j_of_t = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}{tag}.txt", delimiter = " ", usecols = 2)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.scatter(t, j_of_t, marker = '.')#type:ignore
plt.plot(t, j_of_t)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("current $\\langle\\hat j\\rangle$", fontsize=fntsz)
plt.savefig(f"Plots/{namestring}/j_of_t_{namestring}_L={L}_U={U}{tag}.png", bbox_inches='tight')



################################################# integrated current ############################################################
delta_t = t[5] - t[4]
l = len(t)
int_j = 0
int_j_array = np.zeros(l)
for i in range(l):
    int_j += delta_t * j_of_t[i]
    int_j_array[i] = int_j
#print(j_of_t)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.scatter(t, int_j_array, marker = '.')#type:ignore
plt.plot(t, int_j_array)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("integrated current $j_{int}$", fontsize=fntsz)
plt.savefig(f"Plots/{namestring}/j_int_{namestring}_L={L}_U={U}{tag}.png", bbox_inches='tight')
t_local = []
for i in range(1, 200):
    t_local.append(0.1*i)
print(t_local)
################################################ local current ##################################################################
delta_t = 0.1
if model == 'p':
    t_label = [i for i in t_local]
    for i in range(len(t_label)):
        if((t_label[i] % 5) != 0):
            t_label[i] = ''
    time = 0.0
    n_mat = np.zeros((int(t[-1]/delta_t), L-1))
    #n_mat = np.zeros((200, L-1))
    i=0
    while(time <= 20):
        time += delta_t
        #print("%2.6f" % (time))
        #print("teo_{}/local_current/j_loc_L={}_U={}_t={:.6f}.txt".format(namestring, L, U, time))
        n_loc = np.loadtxt("teo_{}/local_current/j_loc_L={}_U={}_t={:.6f}.txt".format(namestring, L, U, time), delimiter = " ", usecols = 2)
        #print(n_mat)
        #for j in range(L-1):
        #    n_mat[j][i] = n_loc[j]
        #print(n_loc)
        n_mat[i] = n_loc
        #print(i)
        #print(n_loc.shape)
        i+=1
    #print(n_mat)
    plt.figure()
    seaborn.heatmap(n_mat.T, xticklabels=t_label)#type:ignore
    plt.xlabel("time $\\tau$")
    plt.ylabel("lattice site $l$")
    plt.tight_layout()
    plt.savefig(f"Plots/{namestring}/j_loc_L={L}_U={U}{tag}.png")
    #print(n_of_t.shape)