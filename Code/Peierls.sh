#!/bin/bash

#this script automates a time evolutions of a with a peierls pulse system
#this script just calculates the states, not the observables
#observables can be measured using the Peierls_meas.sh script

#limit the number of threads 
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

#set exit condition and unset variable treatment
set -u 

SYMMPS=/home/moritz.abel01/MPS/symmps.latest.compressed #thw path of the symmps singularity container
EXECDIR=/opt/symmps/mkl/link/ #the path of the executables inside the singularity container
PRMDIR=/home/moritz.abel01/MPS #the path of models.prm, a file containing the FSMs


#number of sweeps
num_sweeps=30
file_tag=0

#time-evolution parameters
t_start=0.0
t_end=20.1
timestep=0.1

#variables
L=$1 #lattice size
U=$2 #coulomb potential
t=1.0 #hopping strength
delta=-20.0 #alternating potenital strength
a_lattice=1 #lattice constant
e_el=1 #elementary charge
c_vel=3374.85 #speed of light
hbar=1 #Planck's constant
E0=1 #amplitude of the field
k0=0.00126 #wave vector
sigma=10000 #width of the gaussian
t0=10.0 #\tau_0, the time of the maximum of the gaussian

rm log/sym-time-evol-tdvp.prm.log
rm log/sym-time-evol-tdvp_L=${L}_U=${U}.output
rm symmps_files/peierls_lattice_L=${L}_U=${U}.complex
rm symmps_files/peierls_L=${L}_U=${U}.mpo.complex
rm symmps_files/hubbard_lattice_L=${L}_U=${U}.mpo.complex
rm teo_peierls/state.*
set -e

python em-field.py ${L} ${a_lattice} ${e_el} ${c_vel} ${hbar} ${E0} ${k0} ${sigma} ${t0} #this line creates a plot of the vector potential with the given parameters, can be commented out when not wanted

#create a complex lattice file (since time evolution can yield complex values)
singularity exec ${SYMMPS} ${EXECDIR}sym-create-lattice -L ${L} -F c -b "hub-basis" -d 4 -g "n_hub,s_hub" -t "${L},0" -o symmps_files/peierls_lattice_L=${L}_U=${U}.complex -V 2

#create the MPO
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/peierls_lattice_L=${L}_U=${U}.complex -m models.prm -M "ionic_hubbard_peierls_test" -p "U=${U}|t=${t}|delta=${delta}|a_lattice=${a_lattice}|e_el=${e_el}|c_vel=${c_vel}|hbar=${hbar}|E0=${E0}|k0=${k0}|sigma=${sigma}|t0=${t0}" -o symmps_files/peierls_L=${L}_U=${U}.mpo.complex -n H_t -V 2
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/peierls_lattice_L=${L}_U=${U}.complex -m models.prm -M "ionic_hubbard" -p "U=${U}|t=${t}|delta=${delta}" -o symmps_files/hubbard_lattice_L=${L}_U=${U}.mpo.complex -n H -V 2

#perform a groundstate search
singularity exec ${SYMMPS} ${EXECDIR}sym-groundstate-search -i symmps_files/hubbard_lattice_L=${L}_U=${U}.mpo.complex -s $num_sweeps -e 1e-08 -d 1e-08 -E 1e-10 -S "-s 2 -w 10 -N 3 | -s 10 -w 200 -N 5 -e 1e-04 | -w 1000 -N 7" -c SDD -W -V 2 -o gs_peierls/state_L=${L}_U=${U}.symmps

#execute the time evolution
singularity exec ${SYMMPS} ${EXECDIR}sym-time-evol-tdvp -i symmps_files/peierls_L=${L}_U=${U}.mpo.complex -w 2000 -d 1e-10 -k c -t ${t_start} -D ${timestep} -T ${t_end} -I gs_peierls/state_L=${L}_U=${U}.symmps -O H_t -R -s 2site -n 3 -N 50 -E 1e-08 -o teo_peierls/L=${L}_U=${U}_states -V 2

mv log/sym-time-evol-tdvp.output log/sym-time-evol-tdvp_L=${L}_U=${U}.output