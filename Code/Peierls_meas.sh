#!/bin/bash

# do some cleanups
rm log/sym-scalar-product.*
set -u

SYMMPS=/home/moritz.abel01/MPS/symmps.latest.compressed
EXECDIR=/opt/symmps/mkl/link/
PRMDIR=/home/moritz.abel01/MPS


#time evolution parameters
DT=0.1
T_START=0.0
T_END=20.0

#model parameters
L=$1
U=$2
t=1.0

tag=${3:-''}

#rm teo_peierls/BO_parameter/B=_L=${L}_U=${U}${tag}.txt
#rm teo_peierls/current/j_L=${L}_U=${U}${tag}.txt
#rm teo_peierls/local_current/global_operators*
set -e
#construct MPOs for non-local observables
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/peierls_lattice_L=${L}_U=${U}.complex -m observables.prm -M "current" -p "t=${t}" -o symmps_files/current_L=${L}_U=${U}.mpo.complex -n j -V 2
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/peierls_lattice_L=${L}_U=${U}.complex -m observables.prm -M "BO_parameter" -p "L=${L}" -o symmps_files/BO_parameter_L=${L}_U=${U}.mpo.complex -n BO -V 2

#these lines can be activated to measure the local currents
#for (( i=0; i<((L-1)); i++ )); do
#        singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/peierls_lattice_L=${L}_U=${U}.complex -m observables.prm -M "local_current" -p "t=${t}|j=${i}" -o symmps_files/current_loc_L=${L}_U=${U}_j=${i}.mpo.complex -n j_${i} -V 2
#done

#measure expectation values, waiting until the next time-step has been written
for TIME in $(LANG=en seq -f %f $( echo ${T_START}+${DT} | bc ) ${DT} ${T_END})
do
        STATE="teo_peierls/L=${L}_U=${U}_states/state."${TIME}
        while [ ! -e ${STATE} ]
        do
		echo "waiting for file ${STATE}"
                sleep 5
        done
        
        singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/current_L=${L}_U=${U}.mpo.complex -t ${TIME} -O 'j' -k "${STATE}" -d teo_peierls/current
        singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/BO_parameter_L=${L}_U=${U}.mpo.complex -t ${TIME} -O BO -k "${STATE}" -d teo_peierls/BO_parameter
        singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/hubbard_lattice_L=${L}_U=${U}.mpo.complex -t ${TIME} -O H -o 'n_hub' -k "${STATE}" -d teo_peierls

        #these lines can be activated to measure the local current
        #for (( i=0; i<((L-1)); i++ )); do
        #        singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/current_loc_L=${L}_U=${U}_j=${i}.mpo.complex -t ${TIME} -O j_${i} -k "${STATE}" -d teo_peierls/local_current
        #        
        #done
        #mv teo_peierls/local_current/global_operators_imag teo_peierls/local_current/j_loc_L=${L}_U=${U}_t=${TIME}.txt
        #rm teo_peierls/local_current/global_operators_real
        rm ${STATE}
done

set +e

rm teo_peierls/local_operators/n_hub_imag
mv teo_peierls/local_operators/n_hub_real teo_peierls/local_operators/n_hub_real_L=${1}_U=${U}${tag}.txt

rm teo_peierls/current/global_operators_real
mv teo_peierls/current/global_operators_imag teo_peierls/current/j_L=${L}_U=${U}${tag}.txt

rm teo_peierls/BO_parameter/global_operators_imag
mv teo_peierls/BO_parameter/global_operators_real teo_peierls/BO_parameter/BO_L=${L}_U=${U}${tag}.txt

rm teo_peierls/global_operators_imag
mv teo_peierls/global_operators_real teo_peierls/E_L=${L}_U=${U}${tag}.txt

rm symmps_files/current_loc*