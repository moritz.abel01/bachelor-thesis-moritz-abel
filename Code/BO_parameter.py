import string
import numpy as np
import matplotlib.pyplot as plt
import glob
import scipy.optimize as so
#creates a plot of the bond order parameter for different system sizes
fntsz = 15

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=10) # fontsize of the tick labels
plt.rc('ytick', labelsize=10) # fontsize of the tick labels
plt.rc('legend', fontsize=fntsz) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title

def lastline(filename):
    with open(filename, "rb") as f:
        return sum(1 for _ in f)

#set lattice sites for which the BOP is to be plotted
L_list = [32, 64, 128]


plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('potential strength $U$')
plt.ylabel('$\\langle\\hat B\\rangle$')
#plt.title('expectation values of the bond order parameter')

for L in L_list:
    U_list = np.loadtxt(f"/home/moritz.abel01/MPS/hubbard/gs/fe_ih_varied_U_L={L}.txt", delimiter = ' ')
    BO_list = []
    for U in U_list:
        filename = f"/home/moritz.abel01/MPS/hubbard/gs/BO_parameter_fsc_L={L}_U={U}.txt"
        line_number = lastline(filename)
        BO = np.loadtxt(filename, delimiter = ' ', skiprows = line_number - 1, usecols = 2)
        BO_list.append(BO)
        print(filename)
    plt.scatter(U_list, BO_list, label = f"L={L}")
    plt.legend()
    plt.savefig("Plots/BO_parameter.png", bbox_inches='tight')


#test = np.loadtxt("gs/local_operators/n_hub_real_L=10_U=21.0", delimiter = ' ', skiprows=num_lines-1)