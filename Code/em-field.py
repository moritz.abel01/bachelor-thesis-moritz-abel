import numpy as np
import matplotlib.pyplot as plt
import sys
#creates a plot of the vector potential used in the Peierls pulse. This is executed automazically with the right parameters alongside Peierls.sh
fntsz = 15

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=fntsz) # fontsize of the tick labels
plt.rc('ytick', labelsize=fntsz) # fontsize of the tick labels
plt.rc('legend', fontsize=fntsz) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure titlefntsz = 15

# read in the parameters

t = np.linspace(0, 20, 1000)
#system size
L = int(sys.argv[1])

a=float(sys.argv[2])
e=float(sys.argv[3])
c=float(sys.argv[4])
hbar=float(sys.argv[5])
E0=float(sys.argv[6])
k=float(sys.argv[7])
s=float(sys.argv[8])
t0=float(sys.argv[9])

def A(x, t, a=a, e=e, c=c, E0=E0, k=k, s=s, t0=t0):
    return E0/(k*c) * np.exp(-(a*x - c*(t-t0))**2/(s)**2)*np.sin(k*(a*x - c*(t-1)))

def peierls_exponent(x, t, A, e=e, a=a, hbar=hbar):
    return e*a/(2*hbar)*(A(x, t) + A(x+1, t))


#x array
#x = np.linspace(0, L, 1000)
j_list = [1]
#j = np.arange(0,L)
for j in j_list:
    plt.figure()
    plt.grid(which = 'major', alpha = 0.8, color = '#666666')
    plt.minorticks_on()
    plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
    plt.xlabel('time $\\tau$')
    plt.ylabel('field amplitude $A$')
    plt.plot(t, A(j, t))
    plt.savefig(f"Plots/vector_potential_L={L}_j={j}.png", bbox_inches='tight')

    plt.figure()
    plt.grid(which = 'major', alpha = 0.8, color = '#666666')
    plt.minorticks_on()
    plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
    plt.xlabel('time $\tau$')
    plt.ylabel('Peierls exponent')
    plt.title(f'The Peierls exponent')
    plt.plot(t, peierls_exponent(j, t, A))
    plt.savefig(f"Plots/peierls_exponent_L={L}_j={j}.png", bbox_inches='tight')

#print(A(j, time))