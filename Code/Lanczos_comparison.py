import numpy as np
import matplotlib.pyplot as plt
import seaborn
import sys
from scipy.optimize import curve_fit

fntsz = 15

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=fntsz) # fontsize of the tick labels
plt.rc('ytick', labelsize=fntsz) # fontsize of the tick labels
plt.rc('legend', fontsize=fntsz) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title

if (len(sys.argv) == 3):
    tag = ''
elif (len(sys.argv) == 4):
    tag = sys.argv[3]
else:
    print(f"len(sys.argv) = {len(sys.argv)} is not accepted")
    sys.exit(1)

L=int(sys.argv[1])
U=21.42
model = sys.argv[2] # pass p for Peierls substitution and q for quench
mask = [i for i in range(L+2) if i > 1]

if(model == 'p'):
    namestring = 'peierls'
elif(model == 'q'):
    namestring = 'quench'
else: 
    print('unknown model identifyer passed. quitting...')
    sys.exit(1)

L_compare=20
t = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}k=50.txt", delimiter = " ", usecols = 0)
t_compare = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}dt=0.1.txt", delimiter = " ", usecols = 0)
################################################## global current ##############################################################
j_of_t = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}k=50.txt", delimiter = " ", usecols = 2)
j_of_t_compare = np.loadtxt(f"teo_{namestring}/current/j_L={L_compare}_U={U}dt=0.1.txt", delimiter = " ", usecols = 2)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.plot(t_compare, j_of_t_compare, label=f"k = 20", color = 'tab:blue')
#plt.scatter(t_compare, j_of_t_compare, marker = 'x', label=f"dt = 0.1")#type:ignore
plt.scatter(t, j_of_t, marker = '.', label=f"k = 50", color = 'tab:orange')#type:ignore
#plt.plot(t, j_of_t)
#plt.scatter(t_compare, j_of_t_compare, marker = '.', label=f"dt = 0.025")#type:ignore
#plt.plot(t_compare, j_of_t_compare)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("current $\\langle\\hat j\\rangle$", fontsize=fntsz)
plt.legend()
plt.savefig(f"Plots/{namestring}/j_of_t_{namestring}_L={L}_U={U}_Lanczos_comparison.png", bbox_inches='tight')



################################################### difference #################################################################
#print(t[::2])
difference = j_of_t - j_of_t_compare
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.plot(t_compare, difference)
plt.scatter(t_compare, difference, marker = '.')#type:ignore
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("$\\langle\\hat j (k = 50)\\rangle - \\langle\\hat j (k = 20) \\rangle$", fontsize=fntsz)
plt.savefig(f"Plots/{namestring}/j_of_t_{namestring}_L={L}_U={U}_Lanczos_difference.png", bbox_inches='tight')


################################################### integated current ##########################################################
delta_t = t[5] - t[4]
delta_t_compare = t_compare[5] - t_compare[4]
l = len(t)
l_compare = len(t_compare)
int_j = 0
int_j_compare = 0
int_j_array = np.zeros(l)
int_j_array_compare = np.zeros(l_compare)
for i in range(l):
    int_j += delta_t * j_of_t[i]
    int_j_array[i] = int_j
for i in range(l_compare):
    int_j_compare += delta_t_compare * j_of_t_compare[i]
    int_j_array_compare[i] = int_j_compare
#print(j_of_t)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
#plt.scatter(t, int_j_array, marker = '.', label=f"L={L}")#type:ignore
plt.scatter(t, int_j_array, label = f"k = 50", marker = '.', color = 'tab:orange')#type:ignore
plt.plot(t_compare, int_j_array_compare, label=f"k = 20")#type:ignore
#plt.plot(t, int_j_array_compare)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("$j_{int}$", fontsize=fntsz)
plt.legend()
plt.savefig(f"Plots/{namestring}/j_int_{namestring}_Lanczos_comparison.png", bbox_inches='tight')

