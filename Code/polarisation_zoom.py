import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#creates a plot of the polarisation depending on the electric field for small values of E_0


def number_of_lines(filename):
    with open(filename, "rb") as f:
        return sum(1 for _ in f)

def fit_func(x, m, b):
    return m*x + b

fntsz = 14

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
#plt.rc('xtick', labelsize=8) # fontsize of the tick labels
#plt.rc('ytick', labelsize=8) # fontsize of the tick labels
plt.rc('legend', fontsize=fntsz) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title

U=21.42
L_list_el = [100]

colors = dict({'100':'tab:blue', '150':'tab:red', '200':'tab:green'})

E_strings = ['-0.001', '-0.00075', '-0.0005', '-0.00025', '0.0', '0.00025', '0.0005', '0.00075', '0.001']
E_list = [float(E) for E in E_strings]

middle = int(len(E_strings)/2)
fit_array_pos = np.array([E_list[middle +1], E_list[middle+2]])
fit_array_neg = np.array([E_list[middle -2], E_list[middle-1]])
plot_array_pos = np.linspace(0, fit_array_pos[1])
plot_array_neg = np.linspace(fit_array_neg[0], 0)

plt.figure(figsize=(12, 6))
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('strenght of the electric field $E_0$')
plt.ylabel("$\\langle \\hat P \\rangle$")
for i in range(len(L_list_el)):
    L = L_list_el[i]
    polarisation_el = []
    for j in range(len(E_list)):
        E = E_list[j]
        filename = "gs/local_operators/n_el/n_hub_real_L={}_U={}_E={}.txt".format(L, U, E_strings[j])
        print(filename)
        last_line = number_of_lines(filename)
        n_el = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
        #print(np.sum(n_el))

        if(L != len(n_el)):
            #search for errors in the file names
            print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n_el)}")
            continue

        polarisation_op_el = 0
        
        xm = (L-1)/2

        for i in range(L):
            #print(i - xm)
            polarisation_op_el += (i - xm) * n_el[i]
        polarisation_el.append(1/(L) * polarisation_op_el)


    fit_data_pos = [polarisation_el[middle +1], polarisation_el[middle +2]]
    fit_data_neg = [polarisation_el[middle -2], polarisation_el[middle -1]]
    params_pos, params_pos_error = curve_fit(fit_func, fit_array_pos, fit_data_pos)
    params_neg, params_neg_error = curve_fit(fit_func, fit_array_neg, fit_data_neg)
    plt.plot(plot_array_pos, fit_func(plot_array_pos, *params_pos), label=f'Left, m = {params_pos[0]}, b = {params_pos[1]}', color = colors[str(L)])
    plt.plot(plot_array_neg, fit_func(plot_array_neg, *params_neg), label=f'Right, m = {params_neg[0]}, b = {params_neg[1]}', color = colors[str(L)])
    plt.scatter(E_list, polarisation_el, label = f"L = {L}", color = colors[str(L)])
polarisation_el_array = np.array(polarisation_el) #type:ignore
plt.legend()
plt.savefig(f"Plots/polarisation_E-field_zoom_U={U}.png", bbox_inches='tight')