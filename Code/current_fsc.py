import numpy as np
import matplotlib.pyplot as plt
import seaborn
import sys
from scipy.optimize import curve_fit
#creates plots for the comparison of the current for different system sizes
fntsz = 15

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=fntsz) # fontsize of the tick labels
plt.rc('ytick', labelsize=fntsz) # fontsize of the tick labels
plt.rc('legend', fontsize=fntsz) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title

if (len(sys.argv) == 3):
    tag = ''
elif (len(sys.argv) == 4):
    tag = sys.argv[3]
else:
    print(f"len(sys.argv) = {len(sys.argv)} is not accepted")
    sys.exit(1)

L=int(sys.argv[1]) #system size
U=21.42
model = sys.argv[2] # pass p for Peierls substitution and q for quench
mask = [i for i in range(L+2) if i > 1]

if(model == 'p'):
    namestring = 'peierls'
elif(model == 'q'):
    namestring = 'quench'
else: 
    print('unknown model identifyer passed. quitting...')
    sys.exit(1)

L_compare=10
t = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}{tag}dt=0.1.txt", delimiter = " ", usecols = 0)[:200]

################################################## global current ##############################################################
j_of_t = np.loadtxt(f"teo_{namestring}/current/j_L={L}_U={U}{tag}dt=0.1.txt", delimiter = " ", usecols = 2)[:200]
j_of_t_compare = np.loadtxt(f"teo_{namestring}/current/j_L={L_compare}_U={U}{tag}.txt", delimiter = " ", usecols = 2)[:200]
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.scatter(t, j_of_t, marker = '.', label=f"L={L}")#type:ignore
plt.plot(t, j_of_t)
plt.scatter(t, j_of_t_compare, marker = '.', label=f"L={L_compare}")#type:ignore
plt.plot(t, j_of_t_compare)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("current $\\langle\\hat j\\rangle$", fontsize=fntsz)
plt.legend()
plt.savefig(f"Plots/{namestring}/j_of_t_{namestring}_L={L}_U={U}_compare.png", bbox_inches='tight')



################################################# integrated current ############################################################
delta_t = t[5] - t[4]
l = len(t)
int_j = 0
int_j_compare = 0
int_j_array = np.zeros(l)
int_j_array_compare = np.zeros(l)
for i in range(l):
    int_j += delta_t * j_of_t[i]
    int_j_array[i] = 1/L * int_j
    int_j_compare += delta_t * j_of_t_compare[i]
    int_j_array_compare[i] = 1/10 * int_j_compare
#print(j_of_t)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.6, color = '#999999')
plt.scatter(t, int_j_array, marker = '.', label=f"L={L}")#type:ignore
plt.plot(t, int_j_array)
plt.scatter(t, int_j_array_compare, marker = '.', label=f"L = {L_compare}")#type:ignore
plt.plot(t, int_j_array_compare)
plt.xlabel("time $\\tau$", fontsize=fntsz)
plt.ylabel("$j_{int}/L$", fontsize=fntsz)
plt.legend()
plt.savefig(f"Plots/{namestring}/j_int_{namestring}_L={L}_U={U}_compare.png", bbox_inches='tight')

