#!/bin/bash
# This script's purpose is to demonstrate and explain the following functionalities of the SciPAL SymMps tool-kit
# 1. Perform measurements of local and global observables on states that have been created during a groundstate search
# 2. Employ functionality of sym-info executable to extract entanglement entropy of states that have been created during a groundstate search
#
# Dependencies:
#   run demonstration script 'groundstate.sh' in same directory in first place to create MPS files, lattice and MPO
#
# Input arguments:
#   path to compiled SciPAL-SymMps toolkit
#
# General remarks on called executables
#
# sym-scalar-product:
#  datafiles generated by this executable always contain the tag in its first column and the states norm in its second column


# Force OMP-/MKL thread numbers to be one otherwise thread spawning will slow down simulations
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

# clear log files to avoid unnecessary blow-up of output files (each executable will append its output onto an existing output file)
rm log/sym-scalar-product.output
rm log/sym-info.output

# We exit script if an executable returns with error code
set -eu

# just define a shortcut for the passed location of the toolbox ($1 must only define a path to the folder of the tool-kit containing bin (compiled binaries) and link (symbolic links with some reasonable names), e.g., in any singularity container you may pass /opt/symmps/gnu or /opt/symmps/mkl
EXEC_DIR=$1

# We do a while loop increasing a counter variable $N_SWEEP and looping through all files $GS_DIR/state_after_sweep_${N_SWEEP}.symmps
N_SWEEP=1
echo $N_SWEEP
SRC_FILE=/home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_${N_SWEEP}.symmps
echo $SRC_FILE
while [ -f $SRC_FILE ]
do
# we distinguish between the situation of reading the first MPS datafile (N_SWEEP=0) of any other, in the first case we reset all output files by adding -r shortoption
	echo $N_SWEEP
	if [[ $N_SWEEP -eq 1 ]]
	then
# measure <H>, <Sz> and <Sz^2> of current MPS datafile, recreating datafiles using shortoptions:
# 1. -r force recreation of output files
# 2. -n normalize scalarproduct by current MPS norm, in this case this is not absolutely necessary since states are proper normalized during Lanczos groundstate search
# 3. -i define lattice file which needs to contain tensor product Hilbert space (and thereby also local basis operators) and MPO with which groundstate search was performed
# 4. -t define a tag which is used to label dataset in datafile, here we label the data by the value of N_SWEEP
# 5. -O define MPO(s) whose expectation value(s) is/are to be calculated, can be comma-separated list, specified MPO(s) must have been created into passed lattice input-file before
# 6. -o define local operator(s) whose expectations value(s), can be comma-separated list with entries being arbitrary products of local operators
# 7. -k define MPS file which is used to calculate expectation value, if shortoption bra is also passed scalar product is performed with bra/ket being obtained from MPS being passed in shortoptions b/k respectively
# 8. -d define output directory, global observables (MPO expectation values) are written into file global_operators_* while local observables with id <local_op> are written into files local_operators/<local_op>_* and * is wildcard for real/imag
		$EXEC_DIR/link/sym-scalar-product -rni symmps_files/BO_lattice.mpo.real -t $N_SWEEP -O 'BO' -k $SRC_FILE -d gs
	else
# measure <H>, <Sz> and <Sz^2> of current MPS datafile, recreating datafiles using shortoptions:
# 1. -n normalize scalarproduct by current MPS norm, in this case this is not absolutely necessary since states are proper normalized during Lanczos groundstate search
# 2. -i define lattice file which needs to contain tensor product Hilbert space (and thereby also local basis operators) and MPO with which groundstate search was performed
# 3. -t define a tag which is used to label dataset in datafile, here we label the data by the value of N_SWEEP
# 4. -O define MPO(s) whose expectation value(s) is/are to be calculated, can be comma-separated list, specified MPO(s) must have been created into passed lattice input-file before
# 5. -o define local operator(s) whose expectations value(s), can be comma-separated list with entries being arbitrary products of local operators
# 6. -k define MPS file which is used to calculate expectation value, if shortoption bra is also passed scalar product is performed with bra/ket being obtained from MPS being passed in shortoptions b/k respectively
# 7. -d define output directory, global observables (MPO expectation values) are written into file global_operators_* while local observables with id <local_op> are written into files <local_op>_* and * is wildcard for real/imag
		$EXEC_DIR/link/sym-scalar-product -ni symmps_files/BO_lattice.mpo.real -t $N_SWEEP -O 'BO' -k $SRC_FILE -d gs
	fi

# increase counter variable and set next filename
	N_SWEEP=$(( N_SWEEP+1 ))
	SRC_FILE=gs/state_after_sweep_${N_SWEEP}.symmps
done
