#!/bin/bash

#calculates the Groundstate and the bond order Parameter for different system sizes to prepare a finite size scaling

L=(10 20 30 40 60 80 100 140 180 220 260 300)

U=10

t=1.0
delta=20.0


for j in ${L[@]}
do
    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed bash groundstate_hubbard.sh /opt/symmps/mkl/ /home/moritz.abel01/MPS/ $j $U $t $delta

    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed bash meas_groundstate_hubbard.sh /opt/symmps/mkl/

    mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n/n_hub_real_L=${j}_U=${U}.txt
    mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down/n_down_hub_real_L=${j}_U=${U}.txt
    mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up/n_up_hub_real_L=${j}_U=${U}.txt
    mv /home/moritz.abel01/MPS/hubbard/gs/global_operators_real /home/moritz.abel01/MPS/hubbard/gs/BO_parameter_fsc_L=${j}_U=${U}.txt
done

rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_*
rm /home/moritz.abel01/MPS/hubbard/gs/global_operators_imag