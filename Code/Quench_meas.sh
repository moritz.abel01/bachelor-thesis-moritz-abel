#!/bin/bash

#this script is to be executed alongside the Quench.sh script 

# do some cleanups
rm log/sym-scalar-product.*
set -u

SYMMPS=/home/moritz.abel01/MPS/symmps.latest.compressed #thw path of the symmps singularity container
EXECDIR=/opt/symmps/mkl/link/ #the path of the executables inside the singularity container
PRMDIR=/home/moritz.abel01/MPS #the path of models.prm, a file containing the FSMs


#time evolution parameters (have to be the same as in the Quench.sh script)
DT=0.05
T_START=0.0
T_END=10.0

#model parameters
L=$1
U_in=$2
U=$3
t=1.0


rm teo_quench/current/j_L=${L}_U=${U}.txt
rm teo_quench/BO_parameter/BO_L=${L}_U=${U}.txt
set -e
#construct MPOs for non-local observables
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/quench_lattice_L=${L}.complex -m observables.prm -M "current" -p "t=${t}" -o symmps_files/current.mpo.complex -n j -V 2
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/quench_lattice_L=${L}.complex -m observables.prm -M "BO_parameter" -p "L=${L}" -o symmps_files/BO_parameter_L=${L}.mpo.complex -n BO -V 2

#measure expectation values, waiting until the next time-step has been written
singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/current.mpo.complex -O 'j' -o 'n_hub' -k "gs_quench/state_L=${L}_U=${U_in}.symmps" -d teo_quench/current
singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/BO_parameter_L=${L}.mpo.complex -O 'BO' -k "gs_quench/state_L=${L}_U=${U_in}.symmps" -d teo_quench/BO_parameter
for TIME in $(LANG=en seq -f %f $( echo ${T_START}+${DT} | bc ) ${DT} ${T_END})
do
        STATE="teo_quench/L=${L}_U=${U}_states/state."${TIME}
        while [ ! -e ${STATE} ]
        do
        		echo "waiting for file ${STATE}"
                sleep 5
        done
        singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/current.mpo.complex -t ${TIME} -O 'j' -o 'n_hub' -k "${STATE}" -d teo_quench/current
        singularity exec ${SYMMPS} ${EXECDIR}sym-scalar-product -ni symmps_files/BO_parameter_L=${L}.mpo.complex -t ${TIME} -O 'BO' -k "${STATE}" -d teo_quench/BO_parameter
        rm ${STATE}
 done


rm teo_quench/current/local_operators/n_hub_imag
mv teo_quench/current/local_operators/n_hub_real teo_quench/local_operators/n_hub_real_L=${L}_U=${U}.txt
mv teo_quench/current/global_operators_imag teo_quench/current/j_L=${L}_U=${U}.txt
rm teo_quench/current/global_operators_real
mv teo_quench/BO_parameter/global_operators_real teo_quench/BO_parameter/BO_L=${L}_U=${U}.txt
rm teo_quench/BO_parameter/global_operators_imag