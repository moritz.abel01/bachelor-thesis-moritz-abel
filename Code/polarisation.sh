#!/bin/bash
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
# clear log folder to avoid unnecessary blow-up of output files (each executable will append its output onto an existing output file)
rm log/*.output
# remove already existing lattice files to avoid loading of lattice data which we may not want to have
rm symmps_files/hubbard_lattice_L=${L}_U=${U}.*
# We exit script if an executable returns with error code
set -eu


#calculates the local observables on a Hubbard model with added electric field


L=$1 #lattice size
U=21.42
t=1.0
delta=20.0


E=(-0.00075 -0.0005 -0.00025 0.00025 0.0005 0.00075) #set the calues of E_0 for the simulation

# just define a shortcut for the passed location of the toolbox ($1 must only define a path to the folder of the tool-kit containing bin (compiled binaries) and link (symbolic links with some reasonable names), e.g., in any singularity container you may pass /opt/symmps/gnu or /opt/symmps/mkl
EXEC_DIR=/opt/symmps/mkl/
# just define a shortcut for the passed location of the parameter folder in which the finite states machines are defined
PRM_DIR=/home/moritz.abel01/MPS/


for A in ${E[@]}
do
    #create a lattice file
    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed ${EXEC_DIR}/link/sym-create-lattice -L ${L} -F d -b "hub-basis" -d 4 -g "n_hub,s_hub" -t "${L},0" -o symmps_files/hubbard_lattice_L=${L}_U=${U}.real
    #create the Hamiltonian
    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed ${EXEC_DIR}/link/sym-create-mpo --prm_dir $PRM_DIR -i symmps_files/hubbard_lattice_L=${L}_U=${U}.real -m models.prm -M "ionic_hubbard_E_field" -p "U=${U}|t=${t}|delta=${delta}|A=${A}|L=${L}" -o symmps_files/hubbard_lattice_el_L=${L}_U=${U}.mpo.real -n H
    #perform a groundstate search
    singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed ${EXEC_DIR}/link/sym-groundstate-search -i symmps_files/hubbard_lattice_el_L=${L}_U=${U}.mpo.real -s 500 -e 1e-08 -d 1e-08 -E 1e-10 -S "-s 2 -w 10 -N 3 | -s 10 -w 200 -N 5 -e 1e-04 | -w 1000 -N 7" -c SDD -WV 11 -o gs/state_el_L=${L}_U=${U}.symmps

    SRC_FILE=/home/moritz.abel01/MPS/hubbard/gs/state_el_L=${L}_U=${U}.symmps
    
	singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed $EXEC_DIR/link/sym-scalar-product -rni symmps_files/hubbard_lattice_el_L=${L}_U=$U.mpo.real -o 'n_hub' -k $SRC_FILE -d gs


    mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_el/n_hub_real_L=${L}_U=${U}_E=${A}.txt
    rm /home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_*
done

rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_imag
#rm /home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_*