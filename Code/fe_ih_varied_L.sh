#!/bin/bash

#calculates the Groundstate and the bond order Parameter for different system sizes to prepare a finite size scaling
#different local observables can be added 

L=(64) #a list of the lattice sizes that are used for the calculation
#U=(21.0 21.1 21.2 21.3 21.4 21.42 21.44 21.46 21.48 21.5 21.6 21.7 21.8 21.9 22.0)
#U=(21.44 21.46 21.48 21.5 21.6 21.7 21.8 21.9)
U=(21.35) # 21.42 21.46 21.5 21.7 21.9) # a list of the U-values that are used for the calculations
t=1.0 #hopping parameter
delta=20.0 #ionic potential strenght

echo ${U[@]} > /home/moritz.abel01/MPS/hubbard/gs/fe_ih_param_U.txt
echo ${L[@]} > /home/moritz.abel01/MPS/hubbard/gs/fe_ih_param_L.txt


for j in ${L[@]}
do
    for i in ${U[@]}
    do
        singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed bash groundstate_hubbard.sh /opt/symmps/mkl/ /home/moritz.abel01/MPS/ $j $i $t $delta

        singularity exec /home/moritz.abel01/MPS/symmps.latest.compressed bash meas_groundstate_hubbard.sh /opt/symmps/mkl/

#        mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n/n_hub_real_L=${j}_U=${i}.txt
#        mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down/n_down_hub_real_L=${j}_U=${i}.txt
#        mv /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up_hub_real /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up/n_up_hub_real_L=${j}_U=${i}.txt
        mv /home/moritz.abel01/MPS/hubbard/gs/global_operators_real /home/moritz.abel01/MPS/hubbard/gs/BO_parameter_fsc_L=${j}_U=${i}.txt
    done
done

rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_up_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/local_operators/n_down_hub_imag
rm /home/moritz.abel01/MPS/hubbard/gs/state_after_sweep_*
rm /home/moritz.abel01/MPS/hubbard/gs/global_operators_imag