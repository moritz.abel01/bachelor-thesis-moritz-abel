#!/bin/bash
#this script automates a time evolutions of a quenched system
#this script just calculates the states, not the observables
#observables can be measured using the Quench_meas.sh script

#limit the number of threads 
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1


rm symmps_files/quench_lattice.complex
rm symmps_files/quench_initial.mpo.complex
rm symmps_files/quench_final.mpo.complex
rm teo_quench/state.*
set -eu 

SYMMPS=/home/moritz.abel01/MPS/symmps.latest.compressed #thw path of the symmps singularity container
EXECDIR=/opt/symmps/mkl/link/ #the path of the executables inside the singularity container
PRMDIR=/home/moritz.abel01/MPS #the path of models.prm, a file containing the FSMs

#number of sweeps
num_sweeps=30 # number of sweeps in the groundstate search

#time-evolution parameters
t_start=0.0
t_end=10.0
timestep=0.05

#variables/model parameters
L=$1
U_initial=$2
U_final=$3
t=1.0
delta=20.0

#create a complex lattice file (since time evolution can yield complex values)
singularity exec ${SYMMPS} ${EXECDIR}sym-create-lattice -L ${L} -F c -b "hub-basis" -d 4 -g "n_hub,s_hub" -t "${L},0" -o symmps_files/quench_lattice_L=${L}.complex -V 2

#create the MPOs
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/quench_lattice_L=${L}.complex -m models.prm -M "ionic_hubbard" -p "U=${U_initial}|t=${t}|delta=${delta}" -o symmps_files/quench_initial_L=${L}_U=${U_initial}.mpo.complex -n H_initial -V 2
singularity exec ${SYMMPS} ${EXECDIR}sym-create-mpo --prm_dir ${PRMDIR} -i symmps_files/quench_lattice_L=${L}.complex -m models.prm -M "ionic_hubbard" -p "U=${U_final}|t=${t}|delta=${delta}" -o symmps_files/quench_final_L=${L}_U=${U_final}.mpo.complex -n H_final -V 2

#perform a groundstate search on the initial Hamiltonian
singularity exec ${SYMMPS} ${EXECDIR}sym-groundstate-search -i symmps_files/quench_initial_L=${L}_U=${U_initial}.mpo.complex -s $num_sweeps -e 1e-08 -d 1e-08 -E 1e-10 -S "-s 2 -w 10 -N 3 | -s 10 -w 200 -N 5 -e 1e-04 | -w 1000 -N 7" -c SDD -W -V 2 -O H_initial -o "gs_quench/state_L=${L}_U=${U_initial}.symmps"

#time evolution of the roundstate under the final Hamiltonian
singularity exec ${SYMMPS} ${EXECDIR}sym-time-evol-tdvp -i symmps_files/quench_final_L=${L}_U=${U_final}.mpo.complex -w 1000 -d 1e-10 -k c -t ${t_start} -D ${timestep} -T ${t_end} -I gs_quench/state_L=${L}_U=${U_initial}.symmps -O H_final -R -s 2site -n 3 -N 100 -E 1e-08 -o teo_quench/L=${L}_U=${U_final}_states -V 2
