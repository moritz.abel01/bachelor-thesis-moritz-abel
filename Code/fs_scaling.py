import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

#creates the finite size scaling of the bond order parameter and from that plots the BO parameter at L = \infty

fntsz = 15

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=10) # fontsize of the tick labels
plt.rc('ytick', labelsize=10) # fontsize of the tick labels
plt.rc('legend', fontsize=12) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title

def linear(x, m, b):
    return m*x + b 


def lastline(filename):
    with open(filename, "rb") as f:
        return sum(1 for _ in f)

#set lattice sites for which the BOP is to be plotted
L_list = np.loadtxt("/home/moritz.abel01/MPS/hubbard/gs/fe_ih_param_L.txt", delimiter = ' ', dtype = int)
U_list = np.loadtxt("/home/moritz.abel01/MPS/hubbard/gs/fe_ih_param_U.txt", delimiter = ' ')


x_axis = np.linspace(0, 1/L_list[0])
print(L_list)
print(U_list)
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('inverse system size $\\frac{1}{L}$')
plt.ylabel('$\\langle\\hat B \\rangle$')
#plt.title('expectation values of the bond order parameter in a finite size scaling')

intersections = []

for U in U_list:
    BO_list = []
    for L in L_list:
        filename = f"/home/moritz.abel01/MPS/hubbard/gs/BO_parameter_fsc_L={L}_U={U}.txt"
        line_number = lastline(filename)
        BO = np.loadtxt(filename, delimiter = ' ', skiprows = line_number - 1, usecols = 2)
        BO_list.append(BO)
        print(filename)
    params, params_err = curve_fit(linear, 1/L_list, BO_list)
    intersections.append(linear(0, *params))
    plt.scatter(1/L_list, BO_list, label = f"U={U}")
    plt.plot(x_axis, linear(x_axis, *params))
plt.legend()
plt.xlim(0, 1/L_list[0])
plt.savefig("Plots/BO_parameter_fsc2.png", bbox_inches = 'tight')


print(U_list.shape)
print(len(intersections))
plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('potential strength U')
plt.ylabel('expectation value at $L=\\infty$')
plt.title('expectation values of the bond order parameter')
plt.scatter(U_list, intersections)
plt.savefig("Plots/BO_at_L=infty2.png", bbox_inches = 'tight')