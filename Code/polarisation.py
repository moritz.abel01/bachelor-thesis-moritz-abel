import numpy as np
import matplotlib.pyplot as plt

fntsz = 15

plt.rc('font', size=fntsz) # controls default text sizes
plt.rc('axes', titlesize=fntsz) # fontsize of the axes title
plt.rc('axes', labelsize=fntsz) # fontsize of the x and y labels
plt.rc('xtick', labelsize=10) # fontsize of the tick labels
plt.rc('ytick', labelsize=10) # fontsize of the tick labels
plt.rc('legend', fontsize=fntsz) # legend fontsize
plt.rc('figure', titlesize=fntsz) # fontsize of the figure title

#creates a plots of the polarization operator

################################################### plot the polarisation at E=0 in a finite size scaling #############################################################################
#define a function to return the number of lines in a file 
def number_of_lines(filename):
    with open(filename, "rb") as f:
        return sum(1 for _ in f)

#set the parameters
L_list = np.array([6, 8, 10, 12, 16, 18, 20, 30, 40, 50, 60, 80, 100, 120, 140]) #lattice sizes
U = 21.42 # 1st value of the ionic potential
U2 = 10 # 2nd value of the ionic potential
U3 = 50 # 3rd value of the ionic potential

#create empty variables for later use
polarisation_op = 0
polarisation_op_list = []
polarisation_op2 = 0
polarisation_op_list2 = []
polarisation_op3 = 0
polarisation_op_list3 = []


for L in L_list:

    #read in the values, only use data from last sweep
    filename = f"gs/local_operators/n/n_hub_real_L={L}_U={U}.txt"
    last_line = number_of_lines(filename)
    n = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
    filename2 = f"gs/local_operators/n/n_hub_real_L={L}_U={U2}.txt"
    last_line2 = number_of_lines(filename2)
    n2 = np.loadtxt(filename2, delimiter = ' ', skiprows = last_line2 - 1)[2:]

    filename3 = f"gs/local_operators/n/n_hub_real_L={L}_U={U3}.txt"
    last_line3 = number_of_lines(filename3)
    n3 = np.loadtxt(filename3, delimiter = ' ', skiprows = last_line3 - 1)[2:]
    #check for total number of particles
    #print(np.sum(n))


    if(L != len(n)):
        #search for errors in the file names
        print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n)}")
        continue

    #calculate the polarisation
    polarisation_op = 0
    polarisation_op2 = 0
    polarisation_op3 = 0
    xm = (L-1)/2

    for i in range(L):
        #print(i - xm)
        polarisation_op += (i - xm) * n[i]
        polarisation_op2 += (i - xm) * n2[i]
        polarisation_op3 += (i - xm) * n3[i]
    polarisation_op_list.append(1/L * polarisation_op)
    polarisation_op_list2.append(1/L * polarisation_op2)
    polarisation_op_list3.append(1/L * polarisation_op3)
#print(polarisation_op_list)

plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('inverse lattice size $\\frac{1}{L}$')
plt.ylabel('$\\langle \\hat P \\rangle$')
#plt.ylim((0, 0.1))
plt.scatter(1/L_list, polarisation_op_list, label = f"U = {U}")
plt.scatter(1/L_list, polarisation_op_list2, label = f"U = {U2}")
plt.scatter(1/L_list, polarisation_op_list3, label = f"U = {U3}")
plt.legend()
plt.savefig("Plots/Polarisation_fsc.png", bbox_inches='tight')


print("finite size scaling completed!")

################################################### plot the polarisation dependent on the electric field #############################################################################
U=21.42
L_list_el = [50, 100, 150]
#E_strings = ['-0.001', '0.0008', '-0.0006', '-0.0004', '-0.0002', '-0.00015', '-0.0001', '-0.00005', '0.00005', '0.0001', '0.00015', '0.0002', '0.0004', '0.0006', '0.0008', '0.001']
E_strings = ['-0.5', '-0.3', '-0.1', '-0.05', '-0.03', '-0.01', '-0.005', '-0.003', '-0.001', '0.001', '0.003', '0.005', '0.01', '0.03', '0.05', '0.1', '0.3', '0.5']
#E_strings = ['-0.01', '-0.005', '-0.003', '-0.001', '0.001', '0.003', '0.005', '0.01']
E_list = [float(E) for E in E_strings]

#get the zero value of the polarisation
#zero_val = []
#for L in L_list_el:
#    filename = f"gs/local_operators/n_el/n_hub_real_L={L}_U={U}_E=0.0.txt"
#    print(filename)
#    last_line = number_of_lines(filename)
#    n_el = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
#    #print(np.sum(n_el))#

#    if(L != len(n_el)):
#            #search for errors in the file names
#        print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n_el)}")
#        continue

#    polarisation_op_el = 0
            
#    xm = (L-1)/2

#    for i in range(L):
        #print(i - xm)
#        polarisation_op_el += (i - xm) * n_el[i]
#    zero_val.append(1/L * polarisation_op_el)

plt.figure(figsize = (12,8))
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('strenght of the electric field $E_0$')
plt.ylabel('$\\langle \\hat P \\rangle$')
for i in range(len(L_list_el)):
    L = L_list_el[i]
    #z_val = zero_val[i]
    z_val=0
    polarisation_el = []
    for j in range(len(E_list)):
        E = E_list[j]
        filename = "gs/local_operators/n_el/n_hub_real_L={}_U={}_E={}.txt".format(L, U, E_strings[j])
        print(filename)
        last_line = number_of_lines(filename)
        n_el = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
        #print(np.sum(n_el))

        if(L != len(n_el)):
            #search for errors in the file names
            print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n_el)}")
            continue

        polarisation_op_el = 0
        
        xm = (L-1)/2

        for i in range(L):
            #print(i - xm)
            polarisation_op_el += (i - xm) * n_el[i]
            #polarisation_op_el += i * n_el[i]
        polarisation_el.append(1/(L) * polarisation_op_el - z_val)

    plt.scatter(E_list, polarisation_el, label = f"L = {L}")
polarisation_el_array = np.array(polarisation_el) #type:ignore
plt.legend()
plt.savefig(f"Plots/polarisation_E_field_fsc_U={U}.png", bbox_inches='tight')
print("electric field plot completed")

