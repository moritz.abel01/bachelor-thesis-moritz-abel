import numpy as np
import matplotlib.pyplot as plt

#plots the groundstate polarisation in different phases

def number_of_lines(filename):
    with open(filename, "rb") as f:
        return sum(1 for _ in f)


U_list = [0, 21.42, 100]
L_list = [50]
E_list = [-0.5, -0.3, -0.1, -0.05, -0.03, -0.01, -0.005, -0.003, -0.001, -0.0005, -0.0003, -0.0001, 0.0001 ,0.0003, 0.0005, 0.001, 0.003, 0.005, 0.01, 0.03, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5]

plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('strenght of the electric field $E$')
plt.ylabel('$\\langle \\hat P(E) \\rangle')
for U in U_list:
    for L in L_list:
        polarisation_el = []
        for E in E_list:
            filename = f"gs/local_operators/n_el/n_hub_real_L={L}_U={U}_E={E}.txt"
            last_line = number_of_lines(filename)
            n_el = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
            #print(np.sum(n_el))

            if(L != len(n_el)):
                #search for errors in the file names
                print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n_el)}")
                continue

            polarisation_op_el = 0
            
            xm = (L-1)/2

            for i in range(L):
                #print(i - xm)
                polarisation_op_el += (i - xm) * n_el[i]
            polarisation_el.append(1/L * polarisation_op_el)

        plt.scatter(E_list, polarisation_el, label = f"U = {U}, L = {L}")
plt.legend()
plt.savefig("Plots/polarisation_different_phases.png")

E_list_zoom = [E for E in E_list if abs(E) <= 0.001]

plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('strenght of the electric field $E$')
plt.ylabel('$\\langle \\hat P(E) \\rangle')
for U in U_list:
    for L in L_list:
        polarisation_el = []
        for E in E_list_zoom:
            filename = f"gs/local_operators/n_el/n_hub_real_L={L}_U={U}_E={E}.txt"
            last_line = number_of_lines(filename)
            n_el = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
            #print(np.sum(n_el))

            if(L != len(n_el)):
                #search for errors in the file names
                print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n_el)}")
                continue

            polarisation_op_el = 0
            
            xm = (L-1)/2

            for i in range(L):
                #print(i - xm)
                polarisation_op_el += (i - xm) * n_el[i]
            polarisation_el.append(1/L**2 * polarisation_op_el)

        plt.scatter(E_list_zoom, polarisation_el, label = f"U = {U}, L = {L}")
plt.legend()
plt.savefig("Plots/polarisation_different_phases_zoom.png")