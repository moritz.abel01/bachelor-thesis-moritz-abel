#!/bin/bash


# Force OMP-/MKL thread numbers to be one otherwise thread spawning will slow down simulations
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

# clear log folder to avoid unnecessary blow-up of output files (each executable will append its output onto an existing output file)
rm log/*.output

# remove already existing lattice files to avoid loading of lattice data which we may not want to have
rm symmps_files/hubbard_lattice.*

# We exit script if an executable returns with error code
set -eu

# just define a shortcut for the passed location of the toolbox ($1 must only define a path to the folder of the tool-kit containing bin (compiled binaries) and link (symbolic links with some reasonable names), e.g., in any singularity container you may pass /opt/symmps/gnu or /opt/symmps/mkl
EXEC_DIR=$1

# just define a shortcut for the passed location of the parameter folder in which the finite states machines are defined
PRM_DIR=$2

#read in model parameters
L=$3
U=$4
t=$5
delta=$6

# create a lattice file containing defining a S=0-subspace of tensor-product Hilbert space with the following shortoptions:
# 1. -L defines number of lattice sites, i.e., copies of local Hilbert spaces
# 2. -F defines fundamental datatype of the Hilbert space to be double
# 3. -b defines used basis set, here we choose 'custom' basis set which contains the Spin-Basis, to get more information on any basis set <basis_set> run sym-create-lattice -b <basis_set> -G
# 4. -d defines dimension of the local Hilbert space, we want to describe S=1/2 degrees of freedom thus d=2
# 5. -g defines local symmetry generator which is used to construct the global symmetry generator in this case S^z=\sum_{i=1}^{L}S^z_i, note that only the local operator needs to be specified and since they all share the same representation in each individual local Hilbert space the index 'i' is dropped
# 6. -t defines target-symmetry sector for the global symmetry generator, in this case S^z=0
# 7. -o defines output filename for the created lattice (there is no particular naming convention, however you may want to encode the fundamental datatype in the filename)
${EXEC_DIR}/link/sym-create-lattice -L ${L} -F d -b "hub-basis" -d 4 -g "n_hub,s_hub" -t "${L},0" -o symmps_files/hubbard_lattice.real

# create another lattice file containing heisenberg hamiltonian from a finite states machine (FSM) with the following 
# longoptions:
# 1. --prm_dir defines root directory of the parameter folder in which finite size machines are defined
# shortoptions:
# 1. -i defines lattice input file specifying the Hilbertspace on which the MPO is constructed
# 2. -r defines relative path of the file containing the FSM with respect to the value set in prm_dir
# 3. -m defines model filename cpontaining the desired FSM (default value is models.prm so collecting FSMs in files with these names you can drop this option)
# 4. -M defines FSM identifier in model file from which MPO is constructed
# 5. -p sets parameter for FSM used to create MPO
# 6. -o defines output filename for a lattice containing the created MPO
# 7. -n defines identifier of created MPO under which it is stored in output lattice-file
${EXEC_DIR}/link/sym-create-mpo --prm_dir $PRM_DIR -i symmps_files/hubbard_lattice.real -m models.prm -M "ionic_hubbard" -p "U=${U}|t=${t}|delta=${delta}" -o symmps_files/hubbard_lattice.mpo.real -n H
${EXEC_DIR}/link/sym-create-mpo --prm_dir $PRM_DIR -i symmps_files/hubbard_lattice.real -m observables.prm -M "BO_parameter" -p "L=${L}" -o symmps_files/BO_lattice.mpo.real -n BO

# perform groundstate search where a warmup sweep is employed to create an MPS (you can also define MPS input file which will cause the groundstate search to start running on this state) with the following shortoptions:
# 1. -i defines lattice input file containing MPO whose groundstate is to be determined
# 2. -s defines maximally permitted number of sweeps (a single left-to-right or right-to-left sweep counts as one full sweep) after which groundstate search is cancelled if not converged
# 3. -d defines maximally permimtted discarded weight during groundstate search, can be specified for each sweep individually by passing comma-separated list, last value in list is used for the remaining sweeps
# 4. -w defines maximally permitted number of bond states kept during groundstate search, can be specified for each sweep individually by passing comma-separated list, last value in list is used for the remaining sweeps
# 5. -e defines global convergence threshold ε, if relative overall energy gain during in last completed sweep is below ε than groundstate search is exited
# 6. -N defines maximally permitted Krylov basis size in local Lanczos-Solver, can be specified for each sweep individually by passing comma-separated list, last value in list is used for the remaining sweeps
# 7. -E defines local convergence threshold of local Lanczos-Solver, can be specified for each sweep individually by passing comma-separated list, last value in list is used for the remaining sweeps
# 8. -W this flag activates output of states after every sweep into files gs/state_after_sweep_<n_sweep>.symmps
# 9. -V increases verbosity level to also get some timing as well as convergence information during the local Lanczos-optimizations
${EXEC_DIR}/link/sym-groundstate-search -i symmps_files/hubbard_lattice.mpo.real -s 100 -e 1e-08 -d 1e-08 -E 1e-10 -S "-s 2 -w 10 -N 3 | -s 10 -w 200 -N 5 -e 1e-04 | -w 1000 -N 7" -c SDD -WV 11
