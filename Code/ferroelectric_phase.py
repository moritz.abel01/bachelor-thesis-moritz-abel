import numpy as np
import matplotlib.pyplot as plt


#define a function to return the number of lines in a file 
def number_of_lines(filename):
    with open(filename, "rb") as f:
        return sum(1 for _ in f)

#set the parameters
L_list = np.array([6, 8, 10, 12, 16, 18, 20, 30, 40, 50, 60, 80, 100, 120, 140, 150, 160, 180, 256]) #lattice sizes
U = 21.42 # value of the ionic potential
U2 = 21.0

position_op = 0
position_op_list = []
position_op2 = 0
position_op_list2 = []


for L in L_list:
    filename = f"gs/local_operators/n/n_hub_real_L={L}_U={U}.txt"
    last_line = number_of_lines(filename)
    n = np.loadtxt(filename, delimiter = ' ', skiprows = last_line - 1)[2:]
    filename2 = f"gs/local_operators/n/n_hub_real_L={L}_U={U2}.txt"
    last_line2 = number_of_lines(filename2)
    n2 = np.loadtxt(filename2, delimiter = ' ', skiprows = last_line2 - 1)[2:]
    print(np.sum(n))
    if(L != len(n)):
        print(f"Error, lattice sizes don't match!, L = {L}, len(n) = {len(n)}")
        continue
    position_op = 0
    position_op2 = 0
    for i in range(L):
        position_op += (i+1)*n[i]
        position_op2 += (i+1)*n2[i]
    position_op_list.append(1/L**2 * position_op)
    position_op_list2.append(1/L**2 * position_op2)
print(position_op_list)

plt.figure()
plt.grid(which = 'major', alpha = 0.8, color = '#666666')
plt.minorticks_on()
plt.grid(which = 'minor', alpha = 0.4, color = '#999999')
plt.xlabel('potential strength U')
plt.ylabel('expectation value')
plt.title('expectation values of the bond order parameter')
#plt.ylim((0, 0.1))
plt.scatter(1/L_list, position_op_list, label = f"U = {U}")
plt.scatter(1/L_list, position_op_list2, label = f"U = {U2}")
plt.legend()
plt.savefig("Plots/COM_fsc.png")