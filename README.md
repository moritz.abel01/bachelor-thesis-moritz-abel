# Investigation of Possible Photocurrents when Exciting the Bond-ordered Phase of the Ionic Hubbard Model



## Abstract
This thesis considers the possibility of a photocurrent in the ionic Hubbard model.
The model has three phases, a band insulator phase, a Mott inulator phase, and a spontaneously dimerized or bond-ordered phase. The most promising phase when considering photocurrents is the bond-ordered phase, as it has a broken lattice inversion symmetry.
According to the literature, this broken symmetry can lead to a bulk photovoltaic effect, an effect that allows a photocurrent in a homogeneous material, i.e. a material without p-n-junctions. Based on that, an analysis of the system's response to a photoexcitation is done. The excitation is modelled with quantum quenches (sudden changes of parameters) and a semiclassical approach to modelling a laser pulse, a so-called Peierls substitution. The ferroelectric properties of the bond-ordered phase are analysed. This work relies on matrix product states as a method of performing efficient numerical calculations on the one-dimensional system.

## Structure of this repository
The simulations in my thesis were done using the SymMPS toolkit by Sebastian Paeckel and Thomas Köhler. The singularity container with the the toolkit and some examples can be downloaded on https://www.symmps.eu/. The version used in this thesis is from 2023-01-16. The self-written codes used in my thesis can be found in the Code folder. The central files are models.prm and observables.prm, where the Finite State Machines used in the simulations are stored. The bash scripts in the folder were used to automatize the execution of the SymMPS programms. The python scripts were used for processing the data from the MPS calculations and for plotting. Paths in the scripts have to be changed to Your preferred directories.

